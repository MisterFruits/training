.. title:: Gitlab CI
.. meta::
    :author: Victor Cameo Ponz
    :keywords: git, gitlab, ci, gitlabci, web presentation

:css: custom-slides.css

-----

Gitlab CI
=========

.. note::
    Objectifs:
     - comprendre le fonctionnement de la CI Gitlab
     - mettre en place un runner
     - mettre en place la CI pour un projet
     - On va parler de CI et non de CD (pour la bonne raison que j'en ai jamais fait)


-----

Contents
--------

 - General presentation of Gitlab CI
 - Setting up a Gitlab runner
 - Setup a CI
 - Additionnal keywords
 - Gitlab pages
 - Additionnal tips & tricks
 - Available web ressources

.. note::
    On va essayer de mettre en pratique au fur et à mesure. Vous allez avoir besoin de choisir un projet git et on va utiliser une sandbox commune.


-----


General presentation of Gitlab CI
---------------------------------

Why ?

- Tests
- Linter
- Security
- Release
- One limit: your imagination !

.. note::
    Alors pourquoi la CI ? C'est simple, pour automatiser, on peut scripter ce qu'on veut. Ca aide à écrire du code de qualité.


-----


General presentation of Gitlab CI
---------------------------------


 .. image:: images/gitlabci-overview.svg
    :width: 800px

.. note::
    - Clé de voute = instance gitlab
    - Certaines action des utilisateur actione une pipeline qui envoie des jobs sur les runner
    - Chaque job tourne indépendament des autres
    - On va commencer par voir comment mettre en place un runner

-----


Setting up a Gitlab runner
--------------------------

The runner should first be registered into the Gitlab instance. This can be at differents level:
 - project, specific runners
 - group, sub-group
 - gitlab instance, shared runners

In the project/group settings > CI/CD > Runners you will find the URL and access token to register a runner:

 .. image:: images/runner-settings.png
    :width: 400px

.. note::
    - Avant de faire tourner un runner il faut l'enregistrer sur l'instance gitlab, c'est une phase d'identification indipensable
    - les runners peuvent tourner à plusieur niveau dans l'instance gitlab, ça permet de partager ou non des runner.
    - A notre niveau on va pouvoir mettre en place des runner projet et groupe. Seul les admins de l'instance vont pouvoir enregister un shared runner

-----


Setting up a Gitlab runner
--------------------------

On the machine you want to run the runner ``git clone https://dci-gitlab.cines.fr/support/deploy-runner``. This repo contains wrappers to register and run a runner. 

- register:

.. code:: sh

    python3 ./register <TOKEN> <NOM_DU_RUNNER> --site-url <URL>

- run:

.. code:: sh

    nohup python3 run  <NOM_DU_RUNNER> > <NOM_DU_RUNNER>.log 2>&1 &


.. note::
    - Ce repo a été créé pour simplifier les étapes de register et du run quand je débuter avec ces outils.
    - avec le recul je ne sais pas si c'est mieux de l'utiliser mais c'est assez pratique, ça vient avec un gitlab-runner qui tourne sur linux
    - demo sur sandbox, essayez vous aussi



-----


Setting up a Gitlab runner
--------------------------

Project settings > CI/CD > Runners shows available runners to this project:

 .. image:: images/runner-running.png
    :width: 400px

-----


Settup a CI
-----------

With Gitlab CI, the pipeline is defined in the ``.gitlab-ci.yml`` file in the root directory of the git repository.

In this file you can define jobs that will be run in different stages. The CI will run each stage sequentially and jobs of each stage in paralel.

 .. image:: images/pipeline-exemple.png
    :width: 800px


.. note::
    - quand on veut configurer la CI ça se passe dans le gitlabci.yml
    - la dedans on va définir des stage et des jobs. Il faut bien comprendre ce concept pour pouvoir faire dépendre des jobs les uns des autres et parallèliser au maximum la CI
    - c'est du YAML, donc un format de données, en gros des listes et listes de listes

-----


Settup a CI
-----------

Stages are defined this way:

.. code:: yaml

    stages:
      - build
      - test
      - deploy


This three stages are default, when no ``stages`` list is defined they will be available for jobs.


.. note::
    - C'est juste une liste de noms 
    - les stages par défauts sont ceux qui sont affichés. Si on les redéfini pas on peut les utiliser.


-----


Settup a CI
-----------

Jobs are defined this way:

.. code:: yaml

    job1:
        stage: build
        script:
            - echo building
            - make all


.. note::
    - Le job le plus basique c'est un stage et un script sous forme de liste
    - avec ça on peut déjà faire beaucoup de chose avec juste ça, mais vous allez voir que Gitlab fourni énormément de mots clés qui vont permettre beaucoup de controle

-----


Settup a CI
-----------

The most difficult part of Gitlab CI is debugging. Errors in the ``.gitlab-ci.yml`` shows like this:


 .. image:: images/pipeline-error.png
    :width: 600px


The CI linter in CI/CD > Pipeline can be super helpful:

 .. image:: images/ci-linter.png
    :width: 800px

Set of templates here: https://gitlab.com/gitlab-org/gitlab-foss/tree/master/lib/gitlab/ci/templates

You can also execute localy a job of your project using the `gitlab-runner`:

.. code:: bash

    gitlab-runner exec shell <JOB_NAME>

.. note::
    - prennez un de vos projet et essayez de lui ajouter une CI
    - templates gitlab CI: https://gitlab.com/gitlab-org/gitlab-foss/tree/master/lib/gitlab/ci/templates

-----


Additionnal keywords
--------------------

In order to setup a common environement between jobs you can define a ``before_script`` section:


.. code:: yaml

    before_script:
        - export VERSION_NAME=`git describe --dirty --abbrev=4 --tags --always`
        - source setup_env


.. note::
    - la première chose qu'on peut vouloir faire c'est mettre en place un environement commun entre tous les job
    - via le before_script
    - sur les sandbox qu'on maitrise c'est facile d'installer des choses dessus qui seront dispo sur la CI mais sur gitlab.com par exemple, on a que des containers docker basique ou il faut parfois installer les outils qu'on va utiliser

-----



Additionnal keywords
--------------------

You can save output of a job with the ``artifact`` keyword. 


.. code:: yaml

    spack_compile:
        stage: compile
        resource_group: spack
        script:
            - spack install
        artifacts:
             when: always
             paths:
                 - "/tmp/gaia/spack-stage/spack-stage*/*.txt"

Theses files will be availaible :
 - in jobs of following stages, eg. for build executable test results
 - in the Gitlab web interface in CI > pipeline > browse artifacts, eg to consult logs (``when: always`` allow to save artifacts even on failed jobs)  

 .. note::
    - La deuxième chose qu'on veut souvent, c'est récupérer des output des jobs qu'on fait tourner. Soit le résultat d'une compilation, des logs d'erreurs...
    - se fait via les artifacts
    - par défaut ils sont gardés une certaine durée qu'on peut modifier au niveau de l'instance ou directement dans le gitlabci.yaml



-----


Additionnal keywords
--------------------

You can use ``only`` and ``except`` to control when to add jobs to pipelines.

.. code:: yaml

    clean_build:
        stage: clean
        script:
            - python clean_build.py
        except:
            - develop


.. note::
    quand on dit ajouter ou non c'est à dire que le job sera ou ne sera pas présent dans la pipeline (différent du mon clé when qu'on va voir ensuite). 

    Permet de selectionner certaine branche pour faire tourner certains job seulement


-----

Additionnal keywords
--------------------

Use ``when`` to configure the conditions for when jobs run. If not defined in a job, the default value is when: ``on_success``. Possible inputs:

- ``on_success`` (default): Run the job only when all jobs in earlier stages succeed or have allow_failure: true.
- ``manual``: Run the job only when triggered manually.
- ``always``: Run the job regardless of the status of jobs in earlier stages.
- ``on_failure``: Run the job only when at least one job in an earlier stage fails.
- ``delayed``: Delay the execution of a job for a specified duration.
- ``never``: Don’t run the job.


.. code:: yaml

    clean_build:
        stage: clean
        script:
            - python clean_build.py
        when: manual


-----

Additionnal keywords
--------------------


Use ``extends`` to reuse configuration sections. It allows to factorize code in the ``.gitlab-ci.yml``. Possible inputs:
- The name of another job in the pipeline.
- A list (array) of names of other jobs in the pipeline.

.. code:: yaml

    .tests:
      stage: test
      only:
        refs:
          - branches

    test:
      extends: .tests
      script: make test


Another possibility is to use YAML anchors.


.. note::
    J'ai pas mis l'exemple des YAML anchors car je trouve cette facon plus propre. Mais pour qui est déjà à l'aise avec les anchors n'hésitez pas à les utiliser. 

    Vous pouvez aussi allez voir la doc si vous êtes currieux

    Ces templates de jobs peuvent être utilisés en conjonction avec except/only pour builder en debug ou release suivant la branche dans laquelle on est.



-----


Gitlab pages
------------

Gitlab pages allows to publish a static website very easily. It must be activated at the instance level (ask your admin). Then define the ``pages`` jobs so artifacts contains html files:

.. code:: yaml

    pages:
      script:
      - mkdir public
      - echo salut > public/index.html
      artifacts:
        paths:
        - public
      only:
      - master

You setup a static site generator to publish the documentation of your code automatically at each release!


.. note::
    Super pratique pour publier de la doc. On fait ça avec tagada ou l'on décrit les données que l'on génère grace à sphinx



-----

Additionnal tips & tricks
-------------------------

 - trigger a pipeline manualy from the main CI/CD page
 - don't trigger the CI for a specific commit adding ``[ci skip]`` to the commit message
 - change the timeout (default 1h) of jobs in the Project settings > CI > General pipelines section
 - use predefined CI variables: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
 - add variable available to your jobs in the Project settings > CI > Variables sections

-----


Available web ressources
------------------------


 - General documentation: https://docs.gitlab.com/ee/ci/
 - Keyword references: https://docs.gitlab.com/ee/ci/yaml/
 - Predefined CI variables: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
 - Google !

