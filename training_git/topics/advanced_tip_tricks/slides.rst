.. title:: Advanced git + tips&tricks
.. meta::
    :author: Victor Cameo Ponz
    :keywords: git, tips, tricks, advanced, web presentation

:css: custom-slides.css

-----

Advanced git + tips&tricks
==========================

.. note::
    Cette présentation s'adresse à des personne aillant déjà travaillé avec git, on ne reviendra pas sur les bases (clone, commit, push, pull, branches) mais on s'intéressera en particulier aux points suivants.

    Vous allez avoir besoin de git en ligne de commande et python sur votre machine ou une machine de travail.

-----

Contents
--------

 - Handling the working tree
 - Interesting commit arguments 
 - Aliasing git commands
 - Other interesting configurations
 - Merging and merge conflicts
 - Explore history with git blame
 - Explore history with git bisect

.. note::
    1. manipuler les fichier pour le commit et commiter
    2. configurer
    3. merger
    4. explorer l'historique


-----

Handling the working tree
-------------------------

What are the different states of a file in a git repository ?

 .. image:: images/file-states.png
    :width: 800px

.. note::
    - modified
    - untracked
    - staged
    - commited/unmodified (n'apparait pas dans le git status)

-----

Handling the working tree
-------------------------

How to work with this different states ?

 .. image:: images/modify-states.svg
    :width: 800px

.. note::
    - J'ai représenté ces 4 différents état qu'un fichier peut prendre ainsi que les commandes pour passer de l'un à l'autre
    - Voici une petite démo, puis je vous laisse 5 minute pour jouer avec
    - si besoin d'un repo vous pouvez cloner: https://gitlab.com/MisterFruits/training.git essentiellement des fichiers textes

-----

Handling the working tree
-------------------------

How to display diffs ?

 .. image:: images/diff-states.svg
    :width: 800px

.. note::
    - Quand on travail on a besoin de principalement ces deux commande de diff;
    - l'une pour diff ce qui est modifié mais non staged
    - l'autre pour diff ce qui staged


-----

Handling the working tree
-------------------------

Can we stage only some chunks of a file ? 

**YES** with ``git add --patch, -p``. "h" prints the help

 .. image:: images/add-patch.png
    :width: 800px

.. note::
    - Démo, montrer le split et le help
    - 5 minutes pour essayer chez vous, modififez un fichier en plusieurs endroits différents et tentez de n'en stager qu'une partie


-----

Interesting commit arguments
----------------------------

 - ``git commit -m, --message``: DON'T use it, edit with vim instead
 - ``git commit -a, --all``: commit all changed files
 - ``git commit --amend``: amend previous commit with staged files (can be combined with ``-a``)

.. note::
    - ATTENTION avec le amend, seulement sur les commit non pushés
    - Démo pour chacun des arguments
    - 5 minutes pour essayer chez vous, modififez un fichier en plusieurs endroits différents et tentez de n'en stager qu'une partie
    - est-ce que vous en connaissez d'autre ?


-----

Aliasing git commands
---------------------

Add an alias section to the ``~/.gitconfig``

.. code::

  [alias]
    lg = log --stat
    l = log --all --graph --oneline --decorate
    h = help -w
    s = status -sb
    d = diff -w
    dw = diff -w --color-words
    dc = diff -w --cached
    dcw = diff -w --cached --color-words
    unstage = reset HEAD --

.. note::
    - git l =  super efficace pour explorer le graph
    - git s = git status en condensé
    - git d attention au -w qui peut être trompeur
    - --color-word permet de faire des diff par mot au lieu de diff par ligne, pratique dans certains cas
    - QQ'un a d'autres alias pratique (commit par exemple ?)

-----

Other interesting configurations
--------------------------------

- use a completion script for git by adding to your ``.bashrc``:

.. code:: sh

    source /etc/bash_completion.d/git # or
    source /usr/share/bash-completion/completions/git


- Enable global gitignore file 
- Usually ``core.editor`` and ``color.ui`` default to this  

.. code::

  [color]
      ui = auto
  [core]
      excludesfile = ~/.gitignore
      editor = vi


.. note::
    - l'autocompletion c'est la base, commandes, arguments, branches. Souvent déjà présent dans un de ces deux dossier, sinon DL sur le net possible
    - attention au gitignore peut parfois jouer des tour
    - les deux autre sont bon a garder en tete
    - qqun a d'autres settings intéressants ?


-----

Merging and merge conflicts
---------------------------

Key points to keep in mind with ``git merge``:
 - it'll modify the checkouted branch 
 - stay calm, unless you pushed, everything is reversible: ``git merge --abort``
 - use ``git status``, ``git diff`` and ``git log``


.. note::
    - on merge assez peu souvent et il n'y a pas forcément des conflits, c'est donc souvent qqch qu'on apréhende
    - mon conseil, aller doucement et se rappeler de ces 3 fondamentaux

-----

Merging and merge conflicts
---------------------------

Merge conflict exercice: ``git clone https://gitlab.com/MisterFruits/training-merge-conflit.git`` and try to merge ``feature-a`` into ``feature-b``:


 .. image:: images/start-merge-conflict.png
    :width: 800px

.. note::
    - On veut merger feature-a dans feature-b donc on veut modifier feature-b
    - git status nous explique ce qu'il faut faire 
    - attention l'alias git s du chapitre d'avant ne donne pas les instructions


-----

Merging and merge conflicts
---------------------------

``git diff`` will show unmerged paths that should be resolved:

 .. image:: images/unmerged-paths.png
    :width: 400px

 - edit the file manualy to resolve the merge conflict
 - You can use ``git checkout --ours, --theirs <PATH>`` to use one or the other version
 - ``git add`` to mark a resolved path
 - ``git commit`` once every path have been resolved


-----

Merging and merge conflicts
---------------------------

 - Big merges can be complicated, use ``git checkout --conflict=merge <PATH>`` to recreate the conflict output for a path.

 - You can also use ``git checkout --conflict=diff3 <PATH>`` to show the base version of the conflict:

  .. image:: images/conflict-diff3.png
    :width: 400px

-----

Explore history with git blame
------------------------------

``git blame <PATH>`` show the last commit editing every line of a file:

  .. image:: images/git-blame.png
    :width: 800px

 - use it in conjuction with ``git show --patch <COMMIT_ID>`` to see the commit message and other modified files to understand better the meening of this line
 - you also can ask the personn who wrote this line for help (be kind it can be yourself!)


-----

Explore history with git bisect
-------------------------------

``git bisect`` can be used to explore the history in orde to find the origin of an error.

 - ``git bisect start`` will start the process
 - ``git bisect bad`` marks a commit where the error is present
 - ``git bisect good`` marks a commit where the error not present
 - ``git bisect skip`` marks a commit that can not be tested
 - ``git bisect log > bisecting.log`` saves the bisecting state
 - ``git bisect replay bisecting.log`` retrieve the state
 - ``git bisect reset`` stops the bisect process and goes back to the original commit


.. note::
    Git bisect est un outil très puissant, on l'utilise en mode interactif:

-----

Explore history with git bisect
-------------------------------

Exercice: ``git clone https://github.com/bast/git-bisect-exercise``

You can also use a script in order to automate and fasten the process: 


.. code::

    git bisect run ../test.sh

the script should return ``0`` on good commits and a value different from ``0`` on bad ones.

-----

Explore history with git bisect
-------------------------------

Exemple of a test that fits the previous exercice:

.. code:: sh

  #! /bin/bash
  python get_pi.py | grep -q 3.14
