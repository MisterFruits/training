Introduction to Sphinx-doc
==========================

This topic will cover:
 - Why documenting
 - A bit of Sphinx history
 - Carateristics
 - Live demo
 - Sphinx pros and cons
 - Sphinx alternatives

Presentation slides are available here: `Introduction to Sphinx-doc slides`_

.. _Introduction to Sphinx-doc slides: slides/index.html
