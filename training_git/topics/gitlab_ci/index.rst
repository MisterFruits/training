Gitlab CI
=========

This topic will cover:

 - mise en place d'un runner
 - gitlab-ci.yaml: stages et jobs. Débugguer avec le linnter
 - mots clés importants: artifacts, except/only, when, job templates with yaml anchors or extends keyword
 - gitlab pages
 - commit with [ci skip]

Presentation slides are available `here`_

.. _here: slides/index.html
