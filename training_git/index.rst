Coding with Git
===============

This section is dedicated to Git trainings. As definied on the `Git website`_:

    Git is a free and open source distributed version control system designed 
    to handle everything from small to very large projects with speed and efficiency.


.. toctree::
   :maxdepth: 2
   :caption: Available Git trainings topics:
   :glob:

   topics/*/index

Future/Envisoned topics related with this trainings are:
    * WIP


.. _Git website: https://git-scm.com
