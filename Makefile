# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SPHINXPROJ    = Trainings
SOURCEDIR     = .
BUILDDIR      = build

SLIDESTARGETS = $(shell find . -name 'slides.rst' -type f  | sed 's/\.rst//' | sed 's%^.%./build/html%')


# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%.sphinx: Makefile
	$(SPHINXBUILD) -M $(basename $@) "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)


# Slide generation
slides: $(SLIDESTARGETS)

build/html/%/slides: %/slides.rst
	hovercraft $^ $@
