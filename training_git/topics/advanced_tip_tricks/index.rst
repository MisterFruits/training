Advanced git + tips&tricks
==========================

This topic will cover:

 - Handling the working tree
 - Interesting commit arguments 
 - Aliasing git commands
 - Other interesting configurations
 - Merging and merge conflicts
 - Explore history with git blame
 - Explore history with git bisect

Presentation slides are available `here`_

.. _here: slides/index.html
