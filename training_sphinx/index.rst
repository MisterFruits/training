Documenting with Sphinx
=======================

This section is dedicated to Sphinx trainings. As definied on the `Sphinx website`_:

    Sphinx is a tool that makes it easy to create intelligent and beautiful documentation,
    written by Georg Brandl and licensed under the BSD license.


.. toctree::
   :maxdepth: 2
   :caption: Available Sphinx trainings topics:
   :glob:

   topics/*/index

Future/Envisoned topics related with this trainings are:
 * Start a Sphinx project

   * Installing Sphinx
   * Edit index.rst and create a new page
   * Basic Sphinx documentation structure

 * reStructuredText mains assests

   * Titles
   * Pararagraphs
   * Style
   * Lists
   * Links
   * Code blocks
   * Footnotes and citations
   * Common errors/warnings and associated messages

 * Complex Sphinx documentation structure

   * Subfolder
   * Nested ToCs

 * Internationalization with Sphinx - multi language documentation
 * Usefull extentions

Learning material and exercices available:
 * A documentation from scratch with Sphinx
 * Integration with `Gitlab Pages`_
 * Integration with `Github Pages`_
 * Internationalize a documentation
 * Installing and using extentions

Language available:
 * French
 * English

Session history:
 * Summer 2018 at CINES


.. _Sphinx website: http://www.sphinx-doc.org/en/master/
.. _Gitlab Pages: https://docs.gitlab.com/ee/user/project/pages/
.. _Github Pages: https://pages.github.com/
