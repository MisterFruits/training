.. title:: Introduction to Sphinx-doc
.. meta::
    :author: Victor Cameo Ponz
    :keywords: sphinx, sphinx-doc, documentation, web presentation

:css: custom-slides.css

-----

Introduction to Sphinx-doc
==========================

.. note::
    On va commencer par se poser rapidement deux questions qui ont leur importance

-----

Why documenting ? How ?
-----------------------

.. note::
    - Pourquoi et comment est-ce qu'on documente
    - J'ai écrit mon propre avis/définition mais j'ai forcément loupé des choses. On va partir de ça pour pousser un peu plus loin de débat.

-----

Main purpose
************

Writing knowledge down

.. note::
    Le but premier, l'objectif d'une documentation.

-----

- for others
- for you
- to get feedback/contribution

.. note::
  - pour "les autres": c'est à dire expliquer le fonctionnement dans son ensemble aux utilisateurs
  - pour vous: developpeurs, au final on est pas si différent des simples utilisateurs 6 mois après avoir implémenter les fonctionnalités.
  - La documentation est le premier endroit (et l'endroit le plus simple) ou on peu remarquer que quelque chose ne va pas. Que ce soit la documentation qui soit incorrecte/pas assez claire ou que l'implémentation ne suive pas la documentation
  - Qu'en pensez vous ?

-----

Good assets
************

.. note::
    Essayons maintenant de voir qu'est-ce qui fait une bonne documentation

-----

  - answer questions
  - showcase
  - good search functionality
  - easy to edit and maintain
  - collaboration: writers & users

.. note::
    - Répondre aux questions des utilisateurs, avant même qu'ils les formules
    - montrer, donner des exemples. Partie importante de la documentation, par ex pour une documentation d'API le fait de pouvoir montrer du code avec une coloration syntaxique est très important
    - la recherche dans la documentation, fonctionnalité incontournable
    - facile à éditer et à maintenir
    - colaboration: possiblité d'éditer à plusieur (point d'emphase par rapport au DCI), faciliter le feedback des utilisateur, voir leur permettre de proposer des amélioration

-----

Sphinx: Bit of history
----------------------

-----

 - created in 2008 by Geord Brandl
 - under license BSD (open source)
 - initial target is documenting the Python language

 .. note::
     Created in order to deliver documentation

-----

 - currently v1.7.5 and under active development
 - 8 major versions and 67 sub-releases (from 2010 to now)

.. note::
   - v1.0 July 2010 (10 sub-releases)
   - v1.1 October 2011 (3 sub-releases)
   - v1.2 December 2013 (7 sub-releases)
   - v1.3 March 2015 (10 sub-releases)
   - v1.4 March 2016 (11 sub-releases)
   - v1.5 December 2016 (9 sub-releases)
   - v1.6 May 2017 (10 sub-releases)
   - v1.7 February 2018 (7 sub-releases)

------

 - Used by many big project for documentation:

 .. image:: images/blender.png
    :width: 300px
 .. image:: images/cmake.png
    :width: 300px
 .. image:: images/gromacs.png
    :width: 300px
 .. image:: images/python.png
    :width: 300px
 .. image:: images/matplotlib.png
    :width: 300px
 .. image:: images/numpy.png
    :width: 300px

.. note::
  - exemple de quelques projets majeurs
  - selection en rapport avec le HPC

------

- Many other: http://www.sphinx-doc.org/en/master/examples.html
- https://docs.readthedocs.io since 2010, massive open-source documentation

.. note::
  - pas mal d'exemple sur le site de sphinx
  - grosse, énorme, gigantesque base de doc sur Read the doc (en grande partie sphinx)

-----

Characteristics
---------------

-----

 - Input: reStructuredText

.. code:: rest

  What is reStructuredText?
  =========================

  You can highlight text in *italics*
  or, in **bold**. Describing computer
  code `` using fixed space font``.

  Make lists:

  - first item
  - second item

- Output: static HTML/CSS, PDF (via Latex), EPUB, man, text

.. note::
    - kind of improved markdown, ie human readable text.
    - Possible d'utiliser des parser différents du RST: 1 dispo le MarkDown (jamais testé)

-----

Show time !

.. note::
   - reST syntax hightlighted screenshot
   - HTML screenshot/live demo ? : TOC, search bar "NVIDIA", PCP System, codeblocks, images,
   - PDF screenshot

-----

 - HTML output has a search bar
 - ToC, references, acronyms
 - Internationalization via gettext
 - Self linkable - nestable
 - Heavily configurable

.. note::
  Selection des atouts principaux de sphinx-doc
  - Bare de recherche dans le HTML Généré
  - des fonctionnalités d'éditions poussées, moins que le latex mais bcp plus que le MarkDown
  - traduction via le standart gettext (std unix), doc => outil => base de donnée de chunk à traduire => doc multi langage
  - flexibilité et modulabilité via le fait qu'on peut faire des liens entre plusieur doc et même les emboiter
  - configurable presque à volonté: extensions, themes, templates, HTML/Latex customization

-----

Conclusion
----------

-----

---:

 - reStructuredText is more complex that MarkDown
 - no syntax checker
 - extensions hard to write

 .. note::
    - plus complexe mais permet plus de choses (liens, tables...)
    - pas de syntax check: à la génération on voit les erreurs
    - En gros l'usage poussé et la customization requiert de se documenter et d'y passer un peu de temps

-----

+++:

 - is files and text so we can use VCS for history/collaboration
 - reST = readable & rich in feature code format (in opposition to LateX and MarkDown)
 - configurable and customizable: many extensions, themes, infinite possibilities

.. note::
    Répond à une grande partie de ce qui fait une bonne documentation:
      - showcase what we give access to -> code-block
      - answer users questions before they ask
      - good search functionality
      - easy to edit and maintain
      - easy to collaborate for writers AND users

-----

Other alternatives
------------------

- mkdocs: https://github.com/mkdocs Full markdown alternative (possible to use with RtD)
- gitbook: https://www.gitbook.com/ another markdown alternative (paid for multiuser)
- nanoc: https://nanoc.ws/ mix between html and markdown, seems more complex and powerful

-----

- AsciiDoc/AsciiDoctor: https://asciidoctor.org special syntax kind of MarkDown
- DocBook: https://docbook.org/ XML, unreadable but validated
- Doxygen: inside sources only, API documentation




