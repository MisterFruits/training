.. Trainings documentation master file, created by
   sphinx-quickstart on Sun Jul 22 02:20:19 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MisterFruits Trainings
=================================

.. include:: /README.rst

.. toctree::
   :maxdepth: 2
   :caption: List of trainings:
   :glob:

   training_*/index


