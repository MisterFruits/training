This is a collection of trainings. It includes slides and exercices for each topics.

Also sessions and talk are conserved here.

It is licensed under `Creative Commons Attribution-ShareAlike 4.0 International`_.

Any feedback are welcome using the Gitlab integrated `issue tracker`_.


.. _Creative Commons Attribution-ShareAlike 4.0 International: https://creativecommons.org/licenses/by-sa/4.0/
.. _issue tracker: https://gitlab.com/MisterFruits/training/issues
